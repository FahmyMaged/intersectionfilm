@extends('layout.default')

@section('content')

	<section class="services container-fluid">
            <h1>Our Services</h1>
            <a href="services.html" class="row skew-wrap">
                <div class="service-1-bg skewed col-md-4">
                    <div class="overlay"></div>
                    <div class="content">
                        <h2>Production</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla.   
                                <span class="see-more right"> Click for More</span>
                        </p>
                        
                    </div>
                </div>
                <div class="service-2-bg skewed col-md-4">
                    <div class="overlay"></div>
                    <div class="content">
                        <h2>Post Production</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla.   
                            <span class="see-more right"> Click for More</span>
                        </p>
                        
                    </div>
                </div>
                <div class="service-3-bg skewed col-md-4">
                    <div class="overlay"></div>
                    <div class="content">
                        <h2>Rental House</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla. 
                            <span class="see-more right"> Click for More</span>   
                        </p>
                        
                    </div>
                </div>
            </a>
        </section> <!--=====  End of .services  ======-->


        <section class="show-reel container-fluid">
            <h1 class="text-center">Show Reel</h1>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 text-center reel-item-wrap">
                        <a class="show-real-item pop" href="#vidModal-1" data-toggle="modal" data-target="#vidModal-1">
                            <img src="public/images/video-thumb-1.jpg" alt="">
                            <!-- http://img.youtube.com/vi/suzOitvpCD4/0.jpg -->
                            <span class="overlay">
                                <h3>Toshiba Android TV</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, accusantium!</p>
                                <span class="c2a">
                                    Click to watch
                                </span>
                            </span>
                        </a>  
                        <!-- Modal -->
                        <div id="vidModal-1" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                              </div>
                              <div class="modal-body">
                                <video width="100%" height="" controls>
                                  <source src="videos/video-1.mp4" type="video/mp4">
                                  <iframe width="560" height="315" src="https://www.youtube.com/embed/suzOitvpCD4" frameborder="0" allowfullscreen></iframe>
                                  <!-- <source src="movie.ogg" type="video/ogg"> -->
                                    Your browser does not support the video tag.
                                </video>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div> -->
                            </div>

                          </div>
                        </div>  <!--=====  End of modal  ======-->
                    </div>
                    <div class="col-md-4 col-sm-6 text-center reel-item-wrap">
                        <a class="show-real-item pop" href="#vidModal-2" data-toggle="modal" data-target="#vidModal-2">
                            <img src="public/images/video-thumb-2.jpg" alt="">
                            <span class="overlay">
                                <h3>Toshiba New Fridge</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, accusantium!</p>
                                <span class="c2a">
                                    Click to watch
                                </span>
                            </span>
                        </a> 
                        <!-- Modal -->
                        <div id="vidModal-2" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                              </div>
                              <div class="modal-body">
                                <video width="100%" height="" controls>
                                  <source src="videos/video-2.mp4" type="video/mp4">
                                  <!-- <source src="movie.ogg" type="video/ogg"> -->
                                    Your browser does not support the video tag.
                                </video>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div> -->
                            </div>

                          </div>
                        </div>  <!--=====  End of modal  ======-->
                                             
                    </div>
                    <div class="col-md-4 col-sm-6 text-center reel-item-wrap">
                        <a class="show-real-item pop" href="#vidModal-3" data-toggle="modal" data-target="#vidModal-3">
                            <img src="public/images/video-thumb-3.jpg" alt="">
                            <span class="overlay">
                                <h3>Title Here</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, accusantium!</p>
                                <span class="c2a">
                                    Click to watch
                                </span>
                            </span>
                        </a> 
                        <!-- Modal -->
                        <div id="vidModal-3" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modal Header</h4>
                              </div>
                              <div class="modal-body">
                                <video width="100%" height="" controls>
                                  <source src="videos/video-3.mp4" type="video/mp4">
                                  <!-- <source src="movie.ogg" type="video/ogg"> -->
                                    Your browser does not support the video tag.
                                </video>
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div> -->
                            </div>

                          </div>
                        </div>  <!--=====  End of modal  ======-->                       
                    </div>
                </div>
                <div class="right">
                    <a href="reel.html" class="see-more"> See Full List</a>
                </div>
                
            </div> <!--=====  End of container  ======-->
            
        </section> <!--=====  End of .show-reel  ======-->

        <section class="key-people">
            <!-- <div class="container-fluid"> -->
                <h1>Key Prople</h1>
            <!-- </div> -->
            <div class="container-fluid people-wrap">
                <div class="container people-part">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <div class="media">
                              <div class="media-left col-xs-5">
                                  <img class="media-object img-circle" src="public/images/people.jpg" alt="">
                              </div>
                              <div class="media-body">
                                <h3 class="media-heading">Farid Abdel Rahman</h3>
                                <p>
                                    Film Producer and General Manager 
                                </p>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="media">
                              <div class="media-left col-xs-5">
                                  <img class="media-object img-circle" src="public/images/people.jpg" alt="">
                              </div>
                              <div class="media-body">
                                <h3 class="media-heading">Wael Sharcas</h3>
                                <p>
                                    Film Director and Producer, All Artistic divisions and foreign crews and contacts 
                                </p>
                              </div>
                            </div>
                        </div>
                    </div> <!--=====  End of .row  ======-->
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <div class="media">
                              <div class="media-left col-xs-5">
                                
                                  <img class="media-object img-circle" src="public/images/people.jpg" alt="">
                                
                              </div>
                              <div class="media-body">
                                <h3 class="media-heading">Imad Abdel Hamid</h3>
                                <p> Creative Director and Producer </p>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="media">
                              <div class="media-left col-xs-5">
                                
                                  <img class="media-object img-circle" src="public/images/people.jpg" alt="">
                                
                              </div>
                              <div class="media-body">
                                <h3 class="media-heading">Karim Deyab</h3>
                                <p>
                                    Editor and Computer Graphics Designer 
                                </p>
                              </div>
                            </div>
                        </div>
                    </div> <!--=====  End of .row  ======-->
                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <div class="media">
                              <div class="media-left col-xs-5">
                                
                                  <img class="media-object img-circle" src="public/images/people.jpg" alt="">
                                
                              </div>
                              <div class="media-body">
                                <h3 class="media-heading">Hossam Hamed</h3>
                                <p>
                                    Manager, Cine & Video Equipment Hire 
                                </p>
                              </div>
                            </div>
                        </div>
                    </div> <!--=====  End of .row  ======-->  
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <hr>    
                        </div>
                    </div>
                </div> <!--=====  End of people-part  ======-->
            </div>
        </section>
        <section class="clients">
            
            <div class="clients-wrap container">
                <h1>Our Clients</h1>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <img src="public/images/clients-1.png" alt="clients" class="client">
                        <img src="public/images/clients-2.png" alt="clients" class="client">
                        <img src="public/images/clients-3.png" alt="clients" class="client">
                        <img src="public/images/clients-4.png" alt="clients" class="client">
                        <img src="public/images/clients-5.png" alt="clients" class="client">     
                    </div>
                       
                </div>
                
            </div>
        </section>
        

@stop