@extends('layout.default')

@section('content')

	<section class="inner services container">
            <h1>Our Services</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="service-img">
                        <img class="" src="images/service-1.jpg" alt="">
                    </div>
                    
                    <div class="content">
                        <h2>Production</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla.    
                        </p>
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service-img">
                        <img class="" src="images/service-2.jpg" alt="">
                    </div>
                    <div class="content">
                        <h2>Post Production</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla.    
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service-img">
                        <img class="" src="images/service-3.jpg" alt="">
                    </div>
                    <div class="content">
                        <h2>Rental House</h2>
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit veniam aliquam, aspernatur doloremque cupiditate sed blanditiis quaerat neque odio nulla.    
                        </p>
                    </div>
                </div>
            </div>
        </section> <!--=====  End of .services  ======-->

@stop