<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Intersection Film</title>
    <link rel="icon" type="image/png" href="public/images/favicon.png">

    <!-- reset css -->
    {!! HTML::style('public/css/reset.css') !!}
    <!--<link rel="stylesheet" href="public/css/reset.css">

    <!-- Bootstrap CSS -->
    
    {!! HTML::style('public/css/bootstrap/bootstrap.min.css') !!}

    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <!-- bootstrap lightbox css -->
    
    {!! HTML::style('public/css/lightbox/ekko-lightbox.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- custom CSS -->
	
    {!! HTML::style('public/css/style.css') !!}

    <!-- perfix free -->
   
    {!! HTML::script('public/js/prefixfree.min.js') !!}

    <!-- jQuery -->
    <!--<script src="http://code.jquery.com/jquery.js"></script>-->
    
    {!! HTML::script('public/js/jquery1.11/jquery-1.11.0.min.js') !!}
    {!! HTML::script('public/js/jquery1.11/jquery-migrate-1.2.1.min.js') !!}
	
</head>
<body onLoad="init()">
	

    <div id="loading" class="full-screen" style="">
        <div class="loading-wrap full-screen">
            <img src="public/images/showreel.svg" border="0">
        </div>
    </div>

    <script>
     var ld=(document.all); var ns4=document.layers; var ns6=document.getElementById&&!document.all; var ie4=document.all; if (ns4) ld=document.loading; else if (ns6) ld=document.getElementById("loading").style; else if (ie4) ld=document.all.loading.style; function init() { if(ns4){ld.visibility="hidden";} else if (ns6||ie4) ld.display="none"; } 
    </script>  

    <div class="content">
        <header id="landing-scene" class="landing-scene full-screen text-center">
            <object id="logo-wrap" data="" type="">
                <!-- <embed id="logo" src="public/images/Logo.svg" type=""> -->
                <!-- <img src="images/logo-big.png" alt="logo"> -->
            </object>

            <div id="top-navigation">
                <div class="container">
                    <div class="row">
                        <nav class="navbar">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <a class="navbar-brand" href="#">
                                      <img id="header-logo" src="public/images/logo.png" alt="logo">
                                  </a>
                                </div> <!-- /.navbar-header -->
                                <div class="collapse navbar-collapse text-center" id="menu">
                                  <ul class="nav navbar-nav">
                                    <li><a class="active" href="{{URL::to('/')}}/">Home  <span class="sr-only">(current)</span></a></li>
                                    <li><a href="{{URL::to('/')}}/services">Services</a></li>
                                    <li><a href="{{URL::to('/')}}/reel">Show <br> Reel</a></li>
                                    <li><a href="{{URL::to('/')}}/egypt">Why <br> Egypt</a></li>
                                    <li><a href="{{URL::to('/')}}/about">About <br> Us </a></li>
                                    <!-- <li class="dropdown dropup">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <br> Us <span class="caret"></span></a>
                                      <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                      </ul>
                                    </li> -->
                                    <li><a href="#contact">Contact <br> Us</a></li>
                                  </ul>
                                  <a id="demoreel" href="#demo-vid" data-toggle="modal" data-target="#demo-vid" class="btn btn-default navbar-btn navbar-right">Demo Reel <i class="glyphicon glyphicon-play-circle"></i></a>
                                  <div id="demo-vid" class="modal fade" role="dialog">
                                      <div class="modal-dialog modal-lg">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Modal Header</h4>
                                          </div>
                                          <div class="modal-body">
                                            <video width="100%" height="" controls>
                                              <source src="videos/demo-reel.mp4" type="video/mp4">
                                              <!-- <source src="movie.ogg" type="video/ogg"> -->
                                                Your browser does not support the video tag.
                                            </video>
                                          </div>
                                          <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div> -->
                                        </div>

                                      </div>
                                    </div>
                              </div> <!-- /.navbar-collapse -->
                              
                              
                            </div> <!-- End of container-fluid -->
                            
                        </nav>
                    </div>
                </div>
                
            </div>
        </header> <!--=====  End of landing-scene  ======-->
        <script>
            $(window).load(function(){
                logoWrap();
            });
        </script>
        
        @yield('content')
        
    </div> <!--=====  End of .content  ======-->
    <footer>
        <section class="contact" id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 contact-item">
                        <div class="page-header">
                            <h2>Contact Info</h2>
                            
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                              <i class="fa fa-map-marker contact-icon"></i>   
                            </div>
                            <div class="col-xs-9">
                                <p>14 Maamal El Sokkar St. <br>
                                 Garden City, Cairo 1145 <br>
                                 Egypt</p>     
                            </div>
                            
                        </div>
                        <div class="row top-marg">
                            <div class="col-xs-3">
                              <i class="fa fa-phone contact-icon"></i>   
                            </div>
                            <div class="col-xs-9">
                                <p>+202-7946691 <br>
                                  +2-012-2354655</p>     
                            </div>
                            
                        </div>
                        <div class="row top-marg">
                            <div class="col-xs-3">
                              <i class="fa fa-fax contact-icon"></i>   
                            </div>
                            <div class="col-xs-9">
                                <p>+202-7946691</p>     
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-4 contact-item">
                        <div class="page-header">
                            <h2>Contact Form *</h2>
                        </div>
                        <form action="" method="" role="form">
                            <div class="form-group">
                                <div class="input-group">
                                    <label for="" class="input-group-addon">
                                        <span class="fa fa-user"></span>
                                    </label>
                                    <input id="name" type="text" class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label for="" class="input-group-addon">
                                        <span class="fa fa-phone"></span>
                                    </label>
                                    <input id="phone" type="tel" class="form-control" placeholder="Phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label for="" class="input-group-addon">
                                        <span class="fa fa-envelope"></span>
                                    </label>
                                    <input id="mail" type="text" class="form-control" placeholder="e-mail">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea id="message" class="form-control">Type Your Message Here..</textarea>
                            </div>
                            <div class="form-group">
                              <button type="submit" id="submit" class="form-control">Submit</button>  
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-5 contact-item">
                        <div class="page-header">
                            <h2>Find us on map:</h2>
                        </div>
                        <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d893.999346416871!2d31.231477545300645!3d30.031558910211743!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1452372315907" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    
                </div>
                <div class="note">
                    <p>(*) For Rental House Reservation Please Fill The Form With Correct Information and One Of Our Representatives Will Contact You.</p>
                </div>
                <hr>
            </div>
        </section>
        <section class="social text-center">
            <div class="social-wrap">
                <a class="social-icon" href="">
                    <i class="fa fa-facebook fb"></i> 
                </a>
                <a class="social-icon" href="">
                    <i class="fa fa-twitter tw"></i>
                </a>
                <a class="social-icon" href="">
                    <i class="fa fa-youtube yt"></i>
                </a>
            </div>
            <br>
            <div class="social-wrap">
                <a href="">
                    info@intersectionfilm.com
                </a>
            </div>
        </section>
        <section class="subfooter text-center">
            <div class="subfooter-wrap">
                Powered By <a target="_blank" href="http://www.megazoon.com"><img src="public/images/megazoon-logo.png" alt=""></a>    
            </div>
            
        </section>
    </footer>
    
     <!-- =====================================

    Loading Javascript Libraries and custom.js 

    ======================================= -->
    
    

    <!-- slick carousel js -->
    {!! HTML::script('public/js/slick/slick.min.js') !!}
    
    <!-- Bootstrap JavaScript -->
    {!! HTML::script('public/js/bootstrap/bootstrap.min.js') !!}
    <!-- Bootstrap lightbox JavaScript -->
    {!! HTML::script('public/js/lightbox/ekko-lightbox.min.js') !!}
    <!-- custom javascript file -->
    {!! HTML::script('public/js/script.js') !!}

    <!-- =====================================

    End of Loading Javascript Libraries and custom.js 

    =======================================-->   

</body>
</html>