function getWindowHeight() {
    var windowHeight = 0;
    if (typeof(window.innerHeight) == 'number') {
        windowHeight = window.innerHeight;
    } else {
        if (document.documentElement && document.documentElement.clientHeight) {
            windowHeight = document.documentElement.clientHeight;
        } else {
            if (document.body && document.body.clientHeight) {
                windowHeight = document.body.clientHeight;
            }
        }
    }
    return windowHeight;
}

function smoothScroll() {
    $(".navbar a[href='#contact']").on('click', function(event) {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 900, function() {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    });
}

function fullscreen() {
    var winH = getWindowHeight();
    var initWidth = $(window).width();
    if (initWidth > 992) {
        $(".full-screen").each(function() {
            $(this).css({
                height: winH,
            });
        });
    } else {
        $(".full-screen").each(function() {
            $(this).css({
                height: "auto",
            });
        });
    }
    // return false;
}

function getTanDeg(deg) {
    var rad = deg * Math.PI / 180;
    return Math.tan(rad);
}

function skewed() {

    var initWidth = $(window).width();
    var toAdd = Math.ceil(getTanDeg(20) * 320) + 6;
    var newWidth = initWidth + toAdd;
    var $hSubject = $(".services .skew-wrap .skewed");
    if (initWidth > 992) {


        $(".skew-wrap").css({
            width: "" + newWidth + "px"
        });
        $hSubject.addClass("desktop");
        $(".services .skew-wrap .skewed.desktop").mouseenter(function() {
            $(this).addClass("hovered");
            $hSubject.not(".hovered").addClass("unhovered")

        });
        $(".services .skew-wrap .skewed.desktop").mouseleave(function() {
            $(this).removeClass("hovered");
            $hSubject.not(".hovered").removeClass("unhovered")

        });
        // return false;
        // console.log(initWidth);

    } else {
        $(".skew-wrap").css({
            width: "auto"
        });
        $hSubject.removeClass("desktop");
    }


}

function getPos(elm) {
    pos = elm.offset().top;
    return pos;
    // console.log(pos);
}
// var pos = $("#top-navigation").offset().top;
var $nav = $("#top-navigation");

function fixtop() {
    var winWidth = $(window).width();
    if (winWidth > 992) {
        $nav.each(function() {
            var winTop = $(window).scrollTop();
            if (pos < winTop) {
                $(this).addClass("fixtop");
                $(this).find(".dropdown").each(function() {
                    $(this).removeClass("dropup");
                });
            } else {
                $(this).removeClass("fixtop");
                $(this).find(".dropdown").each(function() {
                    $(this).addClass("dropup");
                });
            }
            // console.log(pos);
        });
    } else {
        $("#top-navigation").removeClass("fixtop");
    }

}

function popIn() {
    var delayElm = $(".show-real-item.pop");

    for (var i = 0; i < delayElm.length; i++) {
        delayElm.eq(i).css({
            "animation-delay": (i * 400) + "ms"
        });
    };
    $(".reel-item-wrap").each(function() {
        var elmBottom = $(this).offset().top + $(this).height();
        var winTop = $(window).scrollTop();
        var winBottom = winTop + $(window).height();
        if (elmBottom < winBottom) {
            $(this).find(".show-real-item.pop").each(function() {
                $(this).addClass("reel-in-bounce");
            });
        }
    });
}

function sameHeight($elm) {
    var maxHeight = Math.max.apply(null, $elm.map(function() {
        return $(this).height();
    }).get());
    console.log(maxHeight);
    $elm.each(function() {
        $(this).css({
            "min-height": maxHeight
        });
    });
}

function parallax($elm, frac) {
    var winTop = $(window).scrollTop();

    $elm.css({
        "background-position": "0px " + (1 - (winTop / (frac || 2))) + "px"
    });

}

function logoWrap() {
    $("#logo-wrap").append('<embed id="logo" src="public/images/Logo.svg" type="">');

}
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
$(document).ready(function() {
    fullscreen();
    skewed();
    getPos($nav);
    fixtop();
    smoothScroll();
});
$(window).load(function() {
    popIn();
    sameHeight($(".inner.about-us .thumbnail"));
});
$(window).scroll(function() {
    fixtop();
    popIn();
});
$(window).bind('scroll', function() {
    parallax($("#landing-scene"));
});
$(window).resize(function() {
    sameHeight($(".inner.about-us .thumbnail"));
    fullscreen();
    skewed();
    getPos($nav);
    fixtop();
});